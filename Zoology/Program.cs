﻿using System;

namespace Zoology
{
    class Program
    {
        static void Main(string[] args)
        {
            Animal bigfoot = new Animal("Bigfoot", 150.0);
            Animal polarBear = new Animal("Polar bear", 350.0, "Within arctic circle");
            Animal lion = new Animal("Lion", 80.0, "Africa, South-Asia and South-Europe");

            Animal[] animals = { bigfoot, polarBear, lion };

            foreach (Animal animal in animals)
            {
                Console.WriteLine("---------ANIMAL----------");
                animal.PrintInfo();
                Console.WriteLine("-------------------------\n");
            }
        }
    }
    class Animal
    {
        public string Name { get; private set; }
        public double Weight { get; private set; }
        public string Location { get; set; }

        public Animal(string name, double weight)
        {
            this.Name = name;
            this.Weight = weight;
            this.Location = "unknown";
        }
        public Animal(string name, double weight, string location)
        {
            this.Name = name;
            this.Weight = weight;
            this.Location = location;
        }

        public void PrintInfo()
        {
            Console.WriteLine($"Name: {this.Name}\nWeight: {this.Weight}\nLocation: {this.Location}");
        }
    }
}
